<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'bipure');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'xJ4gsA`Rr%36zK5pJlkh/Bg6YBf_OZvV@ZvYQT`kRIl@V*IFOUCe.d`v~l~sY&e5');
define('SECURE_AUTH_KEY',  '?*c}f!<#aIhb3/a4Yfz~;;M,ty!C$)aQ)b5|I94,2jWpxZ7s21cwbD5`%:4`1ZrX');
define('LOGGED_IN_KEY',    '}Xi@PlEz##?hU9-&pB)P4c@}z*7u;1uO{nA}Wd#M?iage)UioR:i1XOB=ID3wUE0');
define('NONCE_KEY',        'iR>F&LFzUG:t;,zx&=e_E5[e]b0QbgKa.#SqH8rY?_*:Tp_`ZNu)IMfi4BUN[FXf');
define('AUTH_SALT',        '3.^cLDgADvb8tWKD6jFO12090Q;BsD/P~spV/~/i_[@2f-8u|C(~-2_?)/wv(X/4');
define('SECURE_AUTH_SALT', ';SH23EM@8*!1:Yc;z|PWT,OSs6,dC/jM(B?{n8<{RR|kTnn3<3bVKpaV@bbYB-UO');
define('LOGGED_IN_SALT',   'G!Woor+2-aUwG&L2H#VhY%l2+DjbN!OyVlf< QZ!2I>4gL)p_!6>0>KnW[3=^r8d');
define('NONCE_SALT',       'QR]HndrDCd*87v@X4i`@Ed=j8~F)hVa9}%2Sr.d{jZ97J_bTyp| 0R 6zbuk!SA}');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
